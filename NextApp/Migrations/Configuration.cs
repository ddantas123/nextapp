namespace NextApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NextApp.Models.NextAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "NextApp.Models.NextAppContext";
        }

        protected override void Seed(NextApp.Models.NextAppContext context)
        {

            /*
              
            0e0fa1b7-be4d-43cb-9ffd-026daac304bf
            1a1a2684-5bcb-415d-8325-3d3aefd454a4
            99582410-9a41-40f4-a306-c3dc9ccdcc2e
            dd347132-a0f0-493d-962f-70573d6e96cc

             */
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
