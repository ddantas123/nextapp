﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NextApp.Startup))]
namespace NextApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
